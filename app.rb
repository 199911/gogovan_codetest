require 'sinatra/base'

module GameOfLife
	class App < Sinatra::Base
		get "/" do
			send_file File.expand_path('index.html', settings.public_folder)
		end
	end
end
