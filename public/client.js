var uri      = "ws://" + window.document.location.host + "/";
var ws       = new WebSocket(uri);
var color = undefined;
var size = 37
ws.onmessage = function(message) {
	msg = JSON.parse(message.data)
	console.log(msg)
	if(!color && msg.color){
		color = msg.color;
	}else if(msg.cell){
		$('#'+msg.cell).css('background-color',msg.color);
	}
	if(msg.board){
		console.log('board');
		console.log(msg.board)
		for(var i = 0 ; i < size; ++i){
			for(var j = 0; j < size; ++j){
				$('#'+i+'-'+j).css('background-color',msg.board[i][j].color);			
			}
		}
	}
};

$(".game_cell").click(function() {
	// console.log($(this).attr('id'))
	var cell_id = $(this).attr('id') 
	// $(this).css('background-color',color);
	ws.send(JSON.stringify({ 
		cell: cell_id, 
		color: color
	}));
});

$(".pattern_button").click(function(){
	var pattern_id = $(this).attr('id')
	ws.send(JSON.stringify({ 
		pattern: pattern_id, 
		color: color
	}));
})