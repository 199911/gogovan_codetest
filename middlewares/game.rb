require 'faye/websocket'
require 'json'

module GameOfLife
  class ChatBackend
    KEEPALIVE_TIME = 15 # in seconds
    CHANNEL        = "chat-demo"

    def initialize(app)
      @app     = app
      @length = 37
      @clients = []
      # create 2d array, a little bit tricky
      @board = Array.new(@length)
      for i in 0..(@length-1)
        @board[i] = Array.new(@length)
        for j in 0..(@length-1)
          @board[i][j] = {
            :color => '#ffffff'
          }
        end
      end
      # update the board every second
      Thread.new do
        sleep 2
        while true do
          @board = updateBoard()
          sendBoard()
          sleep 1
        end
      end
    end

    def isValid?(x, y)
      return ( 0 <= x ) && ( x <= (@length-1) ) && ( 0 <= y ) && ( y <= (@length-1) )
    end

    def isAlive?(x, y)
      return !@board[x][y][:color].eql?('#ffffff')
    end

    def updateBoard()
      newBoard = Array.new(@length)
      for i in 0..(@length-1)
        newBoard[i] = Array.new(@length)
        for j in 0..(@length-1)
          newBoard[i][j] = {
            :color => updateCell(i, j)
          } 
        end
      end      
      return newBoard
    end

    def sendBoard()
      data = {
        :board => @board,
      }
      json = JSON.generate(data) 
      @clients.each {|client| client.send(json) }
    end

    def updateCell(x,y)
      px = [-1, 0, 1,-1, 1,-1, 0, 1]
      py = [-1,-1,-1, 0, 0, 1, 1, 1]
      cnt = 0
      c = 0
      for i in 0..7
        tx = x + px[i]
        ty = y + py[i]
        if (isValid?(tx,ty))
          if (isAlive?(tx,ty))
            cnt += 1
            c += @board[tx][ty][:color].tr('#','').to_i(16)
          end
        end
      end
      if (cnt > 0) 
        c /= cnt
      end
      if(isAlive?(x,y))
        # Live Cell
        if (cnt < 2)
          # Too lonely
          return '#ffffff'
        elsif (cnt < 4)
          return @board[x][y][:color]
        else
          # Too crowded
          return '#ffffff'
        end  
      else
        # Dead Cell
        if (cnt == 3)
          # reproduce
          return "#%06x" % c
        else
          # remain Dead
          return '#ffffff'
        end
      end
    end

    def call(env)
      if Faye::WebSocket.websocket?(env)
        ws = Faye::WebSocket.new(env, nil, {ping: KEEPALIVE_TIME })
        ws.on :open do |event|
          p [:open, ws.object_id]
          # Give client a random color
          data = {
            :color => "#%06x" % (rand * 0xffffff),
            :board => @board,
          }
          json = JSON.generate(data) 
          ws.send(json)
          @clients << ws
        end

        ws.on :message do |event|
          p [:message, event.data]
          # udpate modal
          data = JSON.parse(event.data)
          if (data["pattern"])
            # random position
            x = (rand * (@length-1) ).to_i
            y = (rand * (@length-1) ).to_i
            p [x, y]
            # set pattern
            if(data["pattern"].eql?("pattern-0"))
              px = [-1, 0, 1]
              py = [0 , 0, 0]
            elsif(data["pattern"].eql?("pattern-1"))
              px = [0 , 1,-1,0 ,1 ]
              py = [-1, 0, 1,1 ,1 ]
            elsif(data["pattern"].eql?("pattern-2"))
              px = [-1, 0, 1,-1, 1,-1, 0, 1]
              py = [-1,-1,-1, 0, 0, 1, 1, 1]
            elsif(data["pattern"].eql?("pattern-3"))
              px = [ 1,-1, 0, 0]
              py = [ 1, 0, 0,-1]
            elsif(data["pattern"].eql?("pattern-4"))              
              px = [-1, 0, 1,-1, 1,-1, 1]
              py = [-1,-1,-1, 0, 0, 1, 1]
            end
            # update modal
            for i in 0..(px.length-1)
              tx = x + px[i]
              ty = y + py[i]
              if (isValid?(tx,ty))
                @board[tx][ty][:color] = data["color"]
              end
            end            
            # send board
            sendBoard()
          end
          if (data["cell"])
            pos = data["cell"].split(/#|-/)
            x = pos[0].to_i
            y = pos[1].to_i
            @board[x][y][:color] = data["color"]
            @clients.each {|client| client.send(event.data) }
          end
        end

        ws.on :close do |event|
          p [:close, ws.object_id, event.code, event.reason]
          @clients.delete(ws)
          ws = nil
        end

      else
        @app.call(env)
      end
    end
  end
end